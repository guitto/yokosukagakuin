
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <title>unsupported | 横須賀学院小学校</title>
  <meta name="description" content="横須賀学院小学校のトップページです。">
  <link rel="stylesheet" href="./css/style.css">
</head>
<body class="p-index">
  <header>
    <div class="l-inner">
      <h1 class="p-index__logo"><img src="./images/logo.svg" alt="Yokosukagakuin Elementaly School"></h1>
    </div>
  </header>
  <main>
    <article class="l-wrapper">
      <div class="l-inner">
        <section>
          <h2 class="p-index__title">このウェブブラウザは現在、<br class="u-sp">当サイトではサポートされておりません。</h2>
          <p class="p-index__text u-mt_s">「横須賀学院小学校のトップページ」をご覧いただくための推奨ブラウザは下記になります。</p>
          <div class="u-mt--m u-tac l-center">
            <p class="p-index__browser">【 推奨ブラウザ 】</p>
            <ul class="p-index__list">
              <li class="p-index__item">
                <img src="./images/chrome.png" alt="chrome">
                <p class="p-index__service">Google Chrome<br><span class="p-index__offer">提供：<a href="https://www.google.co.jp/chrome/" target="_blank">Google</a></span></p>
              </li>
              <li class="p-index__item">
                <img src="./images/edge.png" alt="edge">
                <p class="p-index__service">Edge<br><span class="p-index__offer">提供：<a href="https://www.microsoft.com/ja-jp/edge" target="_blank">Microsoft</a></span></p>
              </li>
              <li class="p-index__item">
                <img src="./images/firefox.png" alt="firefox">
                <p class="p-index__service">Firefox<br><span class="p-index__offer">提供：<a href="https://www.mozilla.org/ja/firefox/new/" target="_blank">Mozilla</a></span></p>
              </li>
              <li class="p-index__item">
                <img src="./images/safari.png" alt="safari">
                <p class="p-index__service">Safari<br><span class="p-index__offer">提供：<a href="https://support.apple.com/ja_JP/downloads/safari" target="_blank">Apple</a></span></p>
              </li>
            </ul>
          </div>
          <p class="p-index__text u-mt_l">お手数ではございますが、推奨ブラウザより再度アクセスをいただけますようお願い致します。</p>
          <p class="p-index__company u-mt_m">横須賀学院小学校
          </p>
        </section>
      </div>
    </article>
  </main>


  <footer id="footer">

</footer>
</body>
</html>
