<?php
//urlの取得
global $url_first;
global $url_second;
global $url_third;
global $pageid;
?>

<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns#">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MCTQJ3C');</script>
  <!-- End Google Tag Manager -->
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name='viewport' content='width=device-width, initial-scale=1, viewport-fit=cover'>
  <title>
    <?php if ($title != "") {
      echo $title . " | " . "横須賀学院小学校";
    } ?>
  </title>
  <meta content="横須賀学院小学校のトップページです" name="description">
  <link rel="shortcut icon" href="./wp/assets/images/common/favicon.ico">

  <!-- OGP -->
  <meta property="og:url" content="https://www.yokosukagakuin.ac.jp/primary/wp/" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="トップページ｜横須賀学院小学校" />
  <meta property="og:description" content="横須賀学院小学校のトップページです" />
  <meta property="og:site_name" content="トップページ｜横須賀学院小学校" />
  <meta property="og:image" content="https://www.yokosukagakuin.ac.jp/primary/wp/assets/images/common/ogp.png" />
  <meta property="og:locale" content="ja_JP"  />

  <!--twitter-->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="https://www.yokosukagakuin.ac.jp/primary/wp/">
  <meta name="twitter:image" content="https://www.yokosukagakuin.ac.jp/primary/wp/assets/images/common/ogp.png" />
  <meta name="twitter:title" content="トップページ｜横須賀学院小学校">
  <meta name="twitter:description" content="横須賀学院小学校のトップページです">
  
  <!-- Stylesheet -->
  <link rel="stylesheet" href="./wp/assets/plugin/swiper/css/swiper.css">
  <link rel="stylesheet" href="./wp/assets/css/style.css?20210818">
  <link rel="stylesheet" href="./wp/assets/css/shame.css"><!-- 緊急対応用　常用しない -->

  <script src="./wp/assets/js/ieunsupported.js"></script>

</head>

<body class="p-index">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MCTQJ3C"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <main class="l-viewport">
    <!-- PC用ヘッダーメニュー -->
    <header class="l-header u-pc" id="js-header">
      <div class="l-header_content l-flex -between">
        <a href="https://www.yokosukagakuin.ac.jp/primary/former-top.html" class="l-header_logo">
          <!-- <img src="./wp/assets/images/yokosukagakuin_logo.svg" id="js-header_logo"> -->
        </a>
        <nav class="l-header_nav">
          <ul class="l-header_dropmenu">
            <li class="l-header_dropmenu-item">
              <a href="#" class="header-link -about"><span class="dropmenu-changeBgcolor">学校について</span></a>
              <ul class="l-header_dropmenu-links">
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/aisatsu.html" class="c-link -bold -md"><span class="-changeBgcolor">挨拶</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/enkakugaiyou.html" class="c-link -bold -md"><span class="-changeBgcolor">沿革</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/kyouikurinen.html" class="c-link -bold -md"><span class="-changeBgcolor">建学の精神</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/kirisutokyou1.html" class="c-link -bold -md"><span class="-changeBgcolor">キリスト教教育</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/4422.html" class="c-link -bold -md"><span class="-changeBgcolor">小中高一貫教育</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/shinro.html" class="c-link -bold -md"><span class="-changeBgcolor">卒業後の進路</span></a></li>
              </ul>
            </li>
            <li class="l-header_dropmenu-item">
              <a href="#" class="header-link -education"><span class="dropmenu-changeBgcolor">教育内容</span></a>
              <ul class="l-header_dropmenu-links">
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/kisogakuryoku.html" class="c-link -bold -md"><span class="-changeBgcolor">基礎学力の充実</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/ikitakyouiku.html" class="c-link -bold -md"><span class="-changeBgcolor">生きた教育</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/English.html" class="c-link -bold -md"><span class="-changeBgcolor">English</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/taiwan.html" class="c-link -bold -md"><span class="-changeBgcolor">台湾国際交流</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/kodomotetsugaku.html" class="c-link -bold -md"><span class="-changeBgcolor">こども哲学</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/shizenkyoushitsu.html" class="c-link -bold -md"><span class="-changeBgcolor">五感で感じる教育</span></a></li>
              </ul>
            </li>
            <li class="l-header_dropmenu-item">
              <a href="#" class="header-link -school"><span class="dropmenu-changeBgcolor">学校生活</span></a>
              <ul class="l-header_dropmenu-links">
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/ichinichi.html" class="c-link -bold -md"><span class="-changeBgcolor">1日の生活</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/nenkangyouji.html" class="c-link -bold -md"><span class="-changeBgcolor">年間行事</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/sisetsu.html" class="c-link -bold -md"><span class="-changeBgcolor">施設</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/anzenkanri.html" class="c-link -bold -md"><span class="-changeBgcolor">安全管理</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/schoolbus.html" class="c-link -bold -md"><span class="-changeBgcolor">スクールバス</span></a></li>
              </ul>
            </li>
            <li class="l-header_dropmenu-item">
              <a href="#" class="header-link -admission"><span class="dropmenu-changeBgcolor">入学案内</span></a>
              <ul class="l-header_dropmenu-links">
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/gakkouannai.html" class="c-link -bold -md"><span class="-changeBgcolor">説明会</span></a></li>
                <li><a href="https://www.yokosukagakuin.ac.jp/primary/annaitetsuzuki.html" class="c-link -bold -md"><span class="-changeBgcolor">入学・転入学試験</span></a></li>
              </ul>
            </li>
            <li class="l-header_dropmenu-item">
              <a href="https://www.yokosukagakuin.ac.jp/primary/access.html" class="header-link -access"><span class="dropmenu-changeBgcolor">アクセス</span></a>
            </li>
          </ul>
        </nav>
        <a href="https://www.facebook.com/Yokosukagakuinelementaryschool/" class="u-mx-s">
          <img src="./wp/assets/images/img_facebook.png" alt="facebook" class="l-header_facebook">
        </a>
      </div>
    </header>

    <!-- SP用ヘッダーメニュー -->
    <header class="l-header u-sp">
      <div class="l-header_content l-flex -between">
        <a href="https://www.yokosukagakuin.ac.jp/primary/former-top.html" class="l-header_logo"></a>
        <a href="https://www.facebook.com/Yokosukagakuinelementaryschool/">
          <img src="./wp/assets/images/img_facebook.png" alt="facebook" class="l-header_facebook">
        </a>

        <button class="l-header_hamburger" id="js-hamburger-button"><span></span></button>
        <div class="l-header_hamburger-cont" id="js-hamburger-cont">

          <ul class="accordion-area">

            <li>
              <section>
                <h3 class="acd-title">学校について</h3>
                <div class="acd-box">
                  <ul class="acordion-content">
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/aisatsu.html" class="acd-link -lg -white -bold"><span class="c-text -color -md -dropShadow">挨拶</span></a>
                  </li>
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/enkakugaiyou.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">沿革</span></a>
                  </li>
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/kyouikurinen.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">建学の精神</span></a>
                  </li>
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/aisatsu.html" class="acd-link -lg -white -bold"><span class="c-text -color -md -dropShadow">キリスト教教育</span></a>
                  </li>
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/4422.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">小中高一貫教育</span></a>
                  </li>
                  <li class="">
                    <a href="https://www.yokosukagakuin.ac.jp/primary/shinro.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">卒業後の進路</span></a>
                  </li>
                  </ul>
                </div>
              </section>
            </li>

            <li>
              <section>
                <h3 class="acd-title">教育内容</h3>
                <div class="acd-box">
                  <ul class="acordion-content">
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/kisogakuryoku.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">基礎学力の充実</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/ikitakyouiku.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">生きた教育</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/English.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">English</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/taiwan.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">台湾国際交流</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/kodomotetsugaku.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">こども哲学</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/shizenkyoushitsu.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">五感で感じる教育</span></a>
                    </li>
                  </ul>
                </div>
              </section>
            </li>

            <li>
              <section>
                <h3 class="acd-title">学校生活</h3>
                <div class="acd-box">
                  <ul class="acordion-content">
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/ichinichi.html" class="acd-link -lg -white -bold"><span class="c-text -color -md -dropShadow">１日の生活</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/nenkangyouji.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">年間行事</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/sisetsu.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">施設</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/anzenkanri.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">安全管理</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/schoolbus.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">スクールバス</span></a>
                    </li>
                  </ul>
                </div>
              </section>
            </li>

            <li>
              <section>
                <h3 class="acd-title">入学案内</h3>
                <div class="acd-box">
                  <ul class="acordion-content">
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/gakkouannai.html" class="acd-link -lg -white -bold"><span class="c-text -color -md -dropShadow">説明会</span></a>
                    </li>
                    <li>
                      <a href="https://www.yokosukagakuin.ac.jp/primary/annaitetsuzuki.html" class="acd-link -white -lg -bold"><span class="c-text -color -md -dropShadow">入学・転入学試験</span></a>
                    </li>
                  </ul>
                </div>
              </section>
            </li>

            <a href="https://www.yokosukagakuin.ac.jp/primary/access.html" class="c-button">
              <li class="c-link -header">
                <p class="c-text -white -big">アクセス<span class="c-arrow -right"></span></p>
              </li>
            </a>

          </ul>
          <!-- <a href="https://www.yokosukagakuin.ac.jp/primary/toiawase.html">
            <button class="c-button -header -bgwhite -hover1 u-mt-xl"><p class="c-text -lg -bold -color">お問い合わせ</p></button>
          </a>
          <a href="https://netty01.lekumo.biz/primaryyokosukagakuin/">
            <button class="c-button -header -bgcolor -hover1 u-mt-sm"><p class="c-text -lg -bold -white">いきいきBlog</p></button>
          </a>
          <a href="https://www.dtps.chc.edu.tw/">
            <button class="c-button -header -bgcolor -hover1 u-mt-sm"><p class="c-text -lg -bold -white">海外姉妹校</p></button>
          </a> -->
        </div>
      </div>

    </header>

    <div class="l-sidebar">
      <div class="l-sidebar_content">
        <a href="https://www.yokosukagakuin.ac.jp/primary/annaitetsuzuki.html" class="l-sidebar_button -toggle">
          <button class="c-button -primary -hover1"><p class="c-text -lg -vertical -white -bold">入学<span class="dot">・</span>転入学出願</p></button>
        </a>
        <a href="https://www.yokosukagakuin.ac.jp/primary/toiawase.html" class="l-sidebar_button">
          <button class="c-button -primary -hover1"><p class="c-text -lg -vertical -white -bold">お問い合わせ</p></button>
        </a>
        <a href="https://www.yokosukagakuin.ac.jp/primary/toiawase.html" class="l-sidebar_button">
          <button class="c-button -secondary -hover1"><p class="c-text -lg -vertical -color -bold">資料請求</p></button>
        </a>
        <button class="l-sidebar_button -modal c-button -hover3" id="modal-1_open" data-micromodal-trigger="modal-1" role="button"><img src="./wp/assets/images/img_info.png"></button>
      </div>
    </div>
