"use strict";

/*==========================
ユーザーエージェント判定
==========================*/
// var _ua = (function(u){
//   return {
//     Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
//     || u.indexOf("ipad") != -1
//     || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
//     || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
//     || u.indexOf("kindle") != -1
//     || u.indexOf("silk") != -1
//     || u.indexOf("playbook") != -1,
//     Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
//     || u.indexOf("iphone") != -1
//     || u.indexOf("ipod") != -1
//     || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
//     || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
//     || u.indexOf("blackberry") != -1,
//     ie:(u.indexOf('msie') != -1 || u.indexOf('trident') != -1),
//     edge:(u.indexOf('edg') != -1 || u.indexOf('edge') == -1),
//     safari:(u.indexOf('safari') != -1 && u.indexOf('edge') == -1 && u.indexOf('chrome') == -1)
//   }
// })(window.navigator.userAgent.toLowerCase());

// if(_ua.Tablet)document.body.classList.add('ua_tab');//タブレットのとき
// if(_ua.Mobile)document.body.classList.add('ua_sp');//スマホのとき
// if(_ua.ie)document.body.classList.add('ua_ie');// IEのとき
// if(_ua.edge)document.body.classList.add('ua_edge');// Edgeのとき
// if(_ua.safari)document.body.classList.add('ua_safari');// safariのとき


window.onload = function() {
  init(); //ヘッダースクロール
  // 画面が中途半端なスクロール位置でリロードされても表示するべきものが表示されるようにするため、ロードですぐに呼び出す
  fadeinEvent();
  // スクロールしたら動くイベントとして用意しておく。スクロールするたびに動くようにする
  window.addEventListener('scroll', fadeinEvent, false);
  // if(!sessionStorage.getItem('disp_popup')) {
  //   sessionStorage.setItem('disp_popup', 'on');
  //   ModalOpen();
  // }
  if(MODAL){
    ModalOpen();
  }
};

/*
モーダル
*/ 
const MODAL = document.getElementById('modal-1');
const MODAL_OPEN_btn = document.getElementById('modal-1_open');
const MODAL_CLOSE_btn = document.getElementById('modal-1_close');

function ModalOpen() {
  MicroModal.show('modal-1', {
    disableFocus: true,
    awaitOpenAnimation: true,
    awaitCloseAnimation: true
  });
  MODAL_OPEN_btn.classList.add('-hide');
}
function ModalClose() {
  MicroModal.close('modal-1');
  MODAL_OPEN_btn.classList.remove('-hide');
}
if(MODAL){
  MODAL_OPEN_btn.onclick = function() {
    ModalOpen();
  };
  MODAL_CLOSE_btn.onclick = function() {
    ModalClose();
  };
}

/*
swiper
*/
let mySwiper_main = new Swiper ('.c-swiper-main', {
  effect: 'fade',
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 4000,
  },
  loop: true,
  speed: 2000,
});

let mySwiper_gallery = new Swiper ('.c-swiper-gallery', {
  loop: true,
  centeredSlides:true,
  slidesPerView: 3,
  autoplay: {
    delay: 0,
    disableOnInteraction: false,
    stopOnLast: false,
  },
  speed: 3000,
}); 

/*
ヘッダースクロール
*/ 
const HEADER = document.getElementById("js-header");
function init() {
  // 変化させる位置
  let pxChange = HEADER.clientHeight;
  window.addEventListener('scroll', function(e){
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    // 高さによってヘッダーを変化させる
    if ( scrollTop > pxChange ) {
      HEADER.classList.add('is-scroll');
    }else{
      HEADER.classList.remove('is-scroll');
    }
  });
}

/*
ハンバーガー
*/
const HAMBURGER_BUTTON = document.getElementById('js-hamburger-button');
const HAMBURGER_CONT = document.getElementById('js-hamburger-cont');
// const HAMBURGER_LINK = document.querySelectorAll('.js-hamburger-link');

const movefun = function( event ) {
  event.preventDefault();
}

HAMBURGER_BUTTON.addEventListener('click', hamburger);
function hamburger() {
  this.classList.toggle('is-active');
  HAMBURGER_CONT.classList.toggle('is-open');
  if (HAMBURGER_CONT.classList.contains('is-open')) {
    window.addEventListener( 'touchmove' , movefun , { passive: false } );
  }else{
    window.removeEventListener( 'touchmove' , movefun, { passive: false } );
  }
}

function closeHamburger() {
  HAMBURGER_BUTTON.classList.remove('is-active');
  HAMBURGER_CONT.classList.remove('is-open');
}
// for (let i = 0; i < HAMBURGER_LINK.length; i++) {
//   HAMBURGER_LINK[i].addEventListener('click', closeHamburger);
// }

/*
トップページへ戻るボタン
*/ 
scrollTop('js-scroll-top', 400);

function scrollTop(el, duration) {
  const target = document.getElementById(el);
  target.addEventListener('click', function() {
    let currentY = window.pageYOffset;
    let step = duration/currentY > 1 ? 10 : 100; 
    let timeStep = duration/currentY * step; 
    let intervalId = setInterval(scrollUp, timeStep);


    function scrollUp(){
      currentY = window.pageYOffset;
      if(currentY === 0) {
          clearInterval(intervalId); 
      } else {
          scrollBy( 0, -step ); 
      }
    }
  });
}

/*
スクロールフェードイン
*/ 
// フェードインの処理をまとめた関数
function fadeinEvent() {
  // getElementsByClassName で、fadein のクラスを持つ要素を取得し配列として保持
  var fadeinClass= Array.prototype.slice.call(document.getElementsByClassName("u-fadein"));

  // 配列の数だけ処理を行う
  fadeinClass.forEach(function( element ) {

      // getBoundingClientRect で要素の位置や幅や高さなどを取得
      var boundingClientRect = element.getBoundingClientRect();

      // スクロールの位置情報（html のスクロールがなければ、body のスクロール）を取得
      var scroll = document.documentElement.scrollTop || document.body.scrollTop;

      // ブラウザウィンドウの ビューポートの高さ
      var windowHeight = window.innerHeight;

      // スクロールの位置が、フェードインしたい要素の位置にいるかどうか判定する
      if (scroll > scroll + boundingClientRect.top - windowHeight + 200){
          // 要素を表示する場合は、要素の透明度を無くし、Y方向の移動距離を無くす。これで表示される
          element.style.opacity = "1";
          element.style.transform = "translateY(0)";
      }
  });
}

// アコーディオンをクリックした時の動作
$('.acd-title').on('click', function() {//タイトル要素をクリックしたら
	$('.acd-box').slideUp(500);//クラス名.boxがついたすべてのアコーディオンを閉じる
    
	var findElm = $(this).next(".acd-box");//タイトル直後のアコーディオンを行うエリアを取得
    
	if($(this).hasClass('close')){//タイトル要素にクラス名closeがあれば
		$(this).removeClass('close');//クラス名を除去    
	}else{//それ以外は
		$('.close').removeClass('close'); //クラス名closeを全て除去した後
		$(this).addClass('close');//クリックしたタイトルにクラス名closeを付与し
		$(findElm).slideDown(500);//アコーディオンを開く
	}
});
