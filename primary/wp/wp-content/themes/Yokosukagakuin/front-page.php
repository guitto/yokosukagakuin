<?php $title = "トップページ" ?>
<?php include('./header.php'); ?>
      
      <div class="p-front-page">

        <!-- 初回アクセス時のポップアップウィンドウ -->
        <?php
          $args = array(
            'numberposts' => 1, //表示したい記事の数
            'post_type' => 'news' //カスタム投稿で作成した投稿タイプ
          );
          $customPosts = get_posts($args);
          if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
        ?>
          <div class="c-modal" id="modal-1" aria-hidden="true">
            <div class="c-modal__overlay" tabindex="-1">
              <div class="c-modal__container" role="dialog" aria-modal="true">
                <!-- お知らせの表示 -->
                <header class="c-modal__header">
                  <div class="c-modal__logo"></div>
                  <button class="c-modal__close" id="modal-1_close" aria-label="Close modal" data-micromodal-close><span class="c-button -close"></span></button>
                </header>
                <div class="c-modal__content">
                  <?php if(get_field('アイキャッチ画像')): ?>
                    <img src="<?php the_field('アイキャッチ画像'); ?>" class="c-modal__img">
                  <?php endif; ?>
                  <div class="c-modal__text c-title -color -center -bold"><?php the_field('メインテキスト'); ?></div>
                  <div class="c-modal__text c-text -color -center -sm"><?php the_field('サブテキスト'); ?></div>
                  <a href="<?php the_field('リンク'); ?>">
                    <button class="c-modal__button c-button -white -bgcolor -bold -hover1 u-mt-m"><?php the_field('ボタンテキスト'); ?></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
        <?php else : //お知らせが無い場合 ?>
          <script>
            // モーダルオープンボタンを非表示
            (function () {
              let MODAL_OPEN = document.getElementById('modal-1_open');
              MODAL_OPEN.classList.add('-hide');
            }());
          </script>
        <?php endif; wp_reset_postdata(); //クエリのリセット ?>

        <!-- せかいのはじめてみつけよう -->
        <section class="p-front-page__top">
                    
          <div class="l-flex">
            <div class="side-content">
              <img src="./wp/assets/images/img_earth_goods.png" class="side-content__img c-image -scaleAnimation">
              <div class="side-content__line"></div>
              <div class="side-content__animation"><span class="c-text -rotate -bold -lg">scroll</span></div>
              <div class="side-content__title c-title">せかいの<br>はじめて<br>みつけよう</div>
            </div>
            
            <!-- トップ画像スライダー -->
            <div class="swiper-container c-swiper-main">
              <div class="swiper-wrapper">
                <div class="swiper-slide"><div class="swiper-img -main1"></div></div>
                <div class="swiper-slide"><div class="swiper-img -main2"></div></div>
                <div class="swiper-slide"><div class="swiper-img -main3"></div></div>
                <div class="swiper-slide"><div class="swiper-img -main4"></div></div>
                <div class="swiper-slide"><div class="swiper-img -main5"></div></div>
              </div>
              <div class="swiper-pagination"></div>
            </div>

          </div>
          
        </section>

        <!-- 学校について -->
        <section class="p-front-page__about">
          <div class="l-content c-box -center u-fadein">
            <img src="./wp/assets/images/img_bika.png" class="c-image -bika -animation1">
            <img src="./wp/assets/images/img_pencil.png" class="c-image -pencil -animation3">
            <div class="c-box_content">
              <h4 class="c-title -color -center -bold">
                ちいさなギモンから<br>世界初の大発見は生まれる
              </h4>
              <p class="c-text -bold -color -space -left u-mt-m">
                横須賀学院小学校は、子どもたちの<br class="u-sp">ちいさなギモンが<br class="u-pc">やがて大きな未来を創る力になると信じています。<br>
                誰かのためになりたいという想いの<br class="u-sp">大きさに対して<br class="u-pc">できることはちいさいかもしれません。<br class="u-pc">
                しかし、それはやがて多くの人の協力を得て<br class="u-pc">大きな光となることも知っています。
              </p>
              <a href="https://www.yokosukagakuin.ac.jp/primary/kyouikurinen.html">
                <button class="c-button -bold -light -color -nowrap -hover2 u-mt-l">学校について</button>
              </a>
            </div>
          </div>
        </section>

        <!-- 学びのポイント -->
        <section class="p-front-page__study">
          <div class="l-content u-fadein">
            <h4 class="l-content__header c-title -color -center">学びのポイント</h4>
            <ul class="l-content__list l-flex -center -wrap">
              <li class="l-content__item c-circle -sinuous1">
                <a href="https://www.yokosukagakuin.ac.jp/primary/English.html" class="c-link">
                  <h3 class="c-title -white -center -ll -bold">
                    <span class="main">であう</span>
                    <small class="sub"><img src="./wp/assets/images/font_encounter.svg" class="c-image -font"></small>
                  </h3>
                  <p class="c-text -white -center -sm u-mt-s">いつも世界は、となりにあるよ。<br>ちがうって、すてきだね。<br>みんなでいっしょに、<br>あたらしい世界をつくろうよ。</p>
                </a>
              </li>
              <li class="l-content__item c-circle -sinuous2">
                <a href="https://www.yokosukagakuin.ac.jp/primary/ikitakyouiku.html" class="c-link">
                  <h3 class="c-title -white -center -ll -bold">
                    <span class="main">問う</span>
                    <small class="sub"><img src="./wp/assets/images/font_wonder.svg" class="c-image -font"></small>
                  </h3>
                  <p class="c-text -white -center -sm u-mt-s">わからなくても、考えてみよう。<br>わからないことも、楽しもう。<br>知識や言葉の力は、<br>大きくはばたく力になるから。</p>
                </a>
              </li>
              <li class="l-content__item c-circle -sinuous3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/kirisutokyou1.html" class="c-link">
                  <h3 class="c-title -white -center -ll -bold">
                    <span class="main">わかちあう</span>
                    <small class="sub"><img src="./wp/assets/images/font_share.svg" class="c-image -font"></small>
                  </h3>
                  <p class="c-text -white -center -sm u-mt-s">みんながみんなを、大切にすること。<br>「相手はどんなふうに思うかな？」<br>いつも考えて、<br>思いを共有しようね。</p>
                </a>
              </li>
              <li class="l-content__item c-circle -sinuous4">
                <a href="https://www.yokosukagakuin.ac.jp/primary/shizenkyoushitsu.html" class="c-link">
                  <h3 class="c-title -white -center -ll -bold">
                    <span class="main">組みあわせる</span>
                    <small class="sub"><img src="./wp/assets/images/font_integrate.svg" class="c-image -font"></small>
                  </h3>
                  <p class="c-text -white -center -sm u-mt-s">「であう」「問う」「わかちあう」<br>たくさん勉強したことを組みあわせて、<br>みたこともない力にしよう。<br>大きな力も、小さな力も、<br>すてきなミライのかけらだよ。</p>
                </a>
              </li>
            </ul>
            <img src="./wp/assets/images/img_light.png" class="c-image -light -animation3">
            <img src="./wp/assets/images/img_candle.png" class="c-image -candle -animation2">
          </div>
        </section>

        <!-- 教育について -->
        <section class="p-front-page__education">
          <div class="l-content c-box -center u-fadein">
          <img src="./wp/assets/images/img_bell.png" class="c-image -bell -animation1">
            <div class="c-box_content">
              <div class="c-title -sm -bold -center -color">ミッションステートメント</div>
              <div class="c-border"></div>
              <div class="c-text -bold -color -space -center">
                キリスト教の信仰に基づく教育によって<br>神の前に誠実に生き 真理を追い求め<br class="u-sp"> 愛と奉仕の精神をもって<br>
                社会に 世界に対して 自らの使命を果たす <br class="u-sp"> 人間の教育を目指します
              </div>
              <div class="c-title -sm -bold -color -center u-mt-l">小中高12年一貫教育</div>
              <div class="c-border"></div>
              <div class="c-text -bold -color -space -center">
                現行の「6・3・3制」を<br>児童の成長過程に適した学習が可能な<br>「4・4・2・2制」へと移行しています。
              </div>
              <a href="https://www.yokosukagakuin.ac.jp/primary/4422.html">
                <button class="c-button -bold -light -color -hover2 -nowrap u-mt-l">教育について</button>
              </a>
            </div>
          </div>
        </section>

        <!-- 学校生活 -->
        <section class="p-front-page__school">
          <div class="l-content u-fadein">
            <h4 class="l-content__header c-title -color -center">学校生活</h4>
            <div class="l-school l-flex -wrap -center u-mt-l">
              <img src="./wp/assets/images/img_abc.png" class="c-image -abc -animation2">
              <img src="./wp/assets/images/img_tobibako.png" class="c-image -tobibako -animation3">
              <button class="list -day c-button -imgbk1 -color -trans -hover3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/ichinichi.html">
                  <img src="./wp/assets/images/school-txtBg.png">
                  <p class="c-text -button -lg -nowrap">1日の生活</p>
                </a>
              </button>
              <button class="list -year c-button -imgbk1 -color -trans -hover3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/nenkangyouji.html">
                  <img src="./wp/assets/images/school-txtBg.png">
                  <p class="c-text -button -lg -nowrap">年間行事</p>
                </a>
              </button>
              <button class="list -facility c-button -imgbk1 -color -trans -hover3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/sisetsu.html">
                  <img src="./wp/assets/images/school-txtBg.png">
                  <p class="c-text -button -lg -nowrap">施　設</p>
                </a>
              </button>
              <button class="list -safe c-button -imgbk1 -color -trans -hover3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/anzenkanri.html">
                  <img src="./wp/assets/images/school-txtBg.png">
                  <p class="c-text -button -lg -nowrap">安全管理</p>
                </a>
              </button>
              <button class="list -buss c-button -imgbk1 -color -trans -hover3">
                <a href="https://www.yokosukagakuin.ac.jp/primary/schoolbus.html">
                  <img src="./wp/assets/images/school-txtBg.png">
                  <p class="c-text -button -lg -nowrap">スクールバス</p>
                </a>
              </button>
              <img src="./wp/assets/images/img_basketball.png" class="c-image -basketball -animation1">
            </div>
          </div>
        </section>

        <!-- 入学案内 -->
        <section class="p-front-page__admission">
          <div class="l-content c-box -center u-fadein">
            <img src="./wp/assets/images/img_palet.png" class="c-image -palet -animation1">
            <img src="./wp/assets/images/img_onpu.png" class="c-image -onpu -animation2">
            <img src="./wp/assets/images/img_rula.png" class="c-image -rula -animation3">
            <div class="c-box_content">
              <h4 class="l-content__header c-title -color -center">入学案内</h4>
              <div class="l-flex -even u-mt-l">
                <button class="l-content__button c-button -imgbk2 -color -trans -hover3">
                  <a href="https://www.yokosukagakuin.ac.jp/primary/gakkouannai.html">
                    <img src="./wp/assets/images/admission-txtBg1.png">
                    <p class="c-text -button -lg">説明会</p>
                  </a>
                </button>
                <button class="l-content__button c-button -imgbk2 -color -trans -hover3">
                  <a href="https://www.yokosukagakuin.ac.jp/primary/nyugakushiken.html">
                    <img src="./wp/assets/images/admission-txtBg2.png">
                    <p class="c-text -button -lg -nowrap">入学<br class="u-sp"><span class="u-pc -inline">・</span>転入学試験</p>
                  </a>
                </button>
              </div>
            </div>
          </div>
        </section>

        <!-- 画像一覧 -->
        <section class="p-front-page__images">
          <div class="u-pc">
            <div class="l-content">
              <div class="l-flex -a-start u-fadein">
                <img src="./wp/assets/images/gallery11.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery2.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery3.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery4.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery5.png" class="l-col-5 c-image -tile">
              </div>
              <div class="l-flex -a-start u-fadein">
                <img src="./wp/assets/images/gallery6.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery7.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery12.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery9.png" class="l-col-5 c-image -tile">
                <img src="./wp/assets/images/gallery10.png" class="l-col-5 c-image -tile">
              </div>
            </div>
          </div>

          <div class="u-sp">
            <div class="l-content u-fadein">
              <div class="swiper-container c-swiper-gallery">
                <div class="swiper-wrapper">
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery11.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery2.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery3.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery4.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery5.png"></div>
                </div>
              </div>
              <div class="swiper-container c-swiper-gallery">
                <div class="swiper-wrapper">
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery6.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery7.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery12.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery9.png"></div>
                  <div class="swiper-slide"><img class="swiper-img" src="./wp/assets/images/gallery10.png"></div>
                </div>
              </div>
            </div>
          </div>

        </section>

        <div class="c-button -totop">
          <button class="c-button -trans -hover3" id="js-scroll-top">
            <img class="" src="./wp/assets/images/img_harinezumi.png">
            <p class="c-text -color -center u-mt-m">To TOP</p>
          </button>
        </div>

      </div>

  </main>

<?php include('./footer.php'); ?>