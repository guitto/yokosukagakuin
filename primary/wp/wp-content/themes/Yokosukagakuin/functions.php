<?php 
// 「投稿」を「ブログ投稿」に変更
function Change_menulabel() {
	global $menu;
	global $submenu;
	$name = 'ブログ投稿';
	$menu[5][0] = $name;
	$submenu['edit.php'][5][0] = $name.'一覧';
	$submenu['edit.php'][10][0] = '新しい'.$name;
}
function Change_objectlabel() {
	global $wp_post_types;
	$name = 'ブログ投稿';
	$labels = &$wp_post_types['post']->labels;
	$labels->name = $name;
	$labels->singular_name = $name;
	$labels->add_new = _x('追加', $name);
	$labels->add_new_item = $name.'の新規追加';
	$labels->edit_item = $name.'の編集';
	$labels->new_item = '新規'.$name;
	$labels->view_item = $name.'を表示';
	$labels->search_items = $name.'を検索';
	$labels->not_found = $name.'が見つかりませんでした';
	$labels->not_found_in_trash = 'ゴミ箱に'.$name.'は見つかりませんでした';
}
add_action( 'init', 'Change_objectlabel' );
add_action( 'admin_menu', 'Change_menulabel' );

// お知らせ投稿の追加
function create_post_type() {
  register_post_type( 'news',  // カスタム投稿ID
    array(
      'label' => 'お知らせ投稿',  // カスタム投稿名(管理画面の左メニューに表示されるテキスト)
      'public' => true,  // 投稿タイプをパブリックにするか否か
      'has_archive' => true,  // アーカイブ(一覧表示)を有効にするか否か
      'menu_position' => 3,  // 管理画面上でどこに配置するか今回の場合は「投稿」の下に配置
      'supports' => array('title')  // 投稿画面でどのmoduleを使うか的な設定
    )
  );
}
add_action( 'init', 'create_post_type' ); // アクションに上記関数をフックする

// 管理画面の不要なメニューを非表示
// ツールバー
function aktk_remove_bar_menus( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'wp-logo' );      // WPロゴ
	$wp_admin_bar->remove_menu( 'updates' );      // 更新
	$wp_admin_bar->remove_menu( 'new-content' );  // 新規追加
	$wp_admin_bar->remove_menu( 'archive' );      // 投稿を表示
	$wp_admin_bar->remove_menu( 'view' );         // 投稿を表示
	$wp_admin_bar->remove_menu( 'new-page' ); 		// 固定ページ
	$wp_admin_bar->remove_menu( 'comments' ); 		// コメント
}
add_action( 'admin_bar_menu', 'aktk_remove_bar_menus', 99 );

// 左メニュー
function remove_menus () {
	remove_menu_page( 'index.php' );                  // ダッシュボード
	// remove_menu_page( 'edit.php' );                // 投稿
	// remove_menu_page( 'upload.php' );              // メディア
	remove_menu_page( 'edit.php?post_type=page' );    // 固定ページ
	remove_menu_page( 'edit-comments.php' );          // コメント
	remove_menu_page( 'themes.php' );                 // 外観
	// remove_menu_page( 'plugins.php' );                // プラグイン
	remove_menu_page( 'users.php' );                  // ユーザー
	// remove_menu_page( 'tools.php' );               // ツール
	// remove_menu_page( 'options-general.php' );        // 設定
	// remove_menu_page( 'edit.php?post_type=acf-field-group' );     // カスタムフィールド
}
add_action('admin_menu', 'remove_menus', 11);

/**
* バージョンアップ通知の非表示
*/
function update_nag_hide() {
	remove_action( 'admin_notices', 'update_nag', 3 );
	remove_action( 'admin_notices', 'maintenance_nag', 10 );
}
add_action( 'admin_init', 'update_nag_hide' );

add_image_size( 'blog_list',100,66,true);
$tmp_url='/primary/wp/wp-content/themes/original/img';


//flush_rewrite_rules( false );

add_action( 'init', 'mytheme_init' );
if ( ! function_exists( 'mytheme_init' ) ) {
	function mytheme_init() {
		global $wp_rewrite;
		$wp_rewrite->use_trailing_slashes = false;
		$wp_rewrite->page_structure = $wp_rewrite->root . '%pagename%.html';
		//一度だけ呼び出せばよいので１回実行したらコメントアウト。パーマリンク設定→保存でも同様の効果。
		flush_rewrite_rules( false );
	}
}
function catch_that_image() {
	global $post, $posts;
	global $tmp_url;
	$first_img = '';
	ob_start();
	ob_end_clean();
	
	//$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	//$first_img = $matches [1] [0];
	
	$output = preg_match('/<img.*?src=[\'"]([^\'"]+)[\'"].*>/', $post->post_content, $matches);
	$first_img = $matches [1];

	if(empty($first_img)){ //Defines a default image
		$first_img = $tmp_url . "/blog/noimage.jpg";
	}
	return $first_img;
}
/*  descriptionの設定  */

//pタグを削除する
remove_filter('term_description','wpautop');

function get_meta_description() {
	global $post;
	$description = "";
	if ( is_home() ) {
		// ホームでは、ブログの説明文を取得
		$description = get_bloginfo( 'description' );
	}
	elseif ( is_category() ) {
		// カテゴリーページでは、カテゴリーの説明文を取得
		$description = category_description();
	}
	elseif ( is_single() ) {
		if ($post->post_excerpt) {
			// 記事ページでは、記事本文から抜粋を取得
			$description = $post->post_excerpt;
		} else {
		// post_excerpt で取れない時は、自力で記事の冒頭100文字を抜粋して取得
		$description = strip_tags($post->post_content);
		$description = str_replace("\n", "", $description);
		$description = str_replace("\r", "", $description);
		$description = mb_substr($description, 0, 100) . "...";
		}
	} else {
	;
	}
	
	return $description;
	}
	
	// echo meta description tag
	function echo_meta_description_tag() {
		if ( is_home() || is_category() || is_single() ) {
			echo '<meta name="description" content="' . get_meta_description() . '" />' . "\n";
		}
	}
//省略文字数の変更　55文字→100文字
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
	return 200;
}	

//抜粋の文末を・・・【続きを読む】でリンク
function new_excerpt_more($post) {
    return  '<a class="detail_link" href="'. get_permalink($post->ID) . '">' . '・・・【続きを読む】' . '</a>';  
}  
add_filter('excerpt_more', 'new_excerpt_more');

//ブログのカテゴリー名を出力
function show_category($tmpCats) {
	foreach ($tmpCats as $tmpCat) {
		$tmpCategory.="<span class='blog_category ". $tmpCat->slug. "'>".$tmpCat->name . "</span> ";
	}
	return rtrim($tmpCategory);
}
