<!DOCTYPE html>
<html lang="ja">

<head>
  <?php
  // head内の値をセット
  $title = "タイトルが入ります（ページ名）";
  $keywords = "キーワードが入ります";
  $description = "ディスクリプションが入ります";
  ?>
  <?php include './head.php'; ?>
</head>

<body class="p-page-name">
  <?php include './header.php'; ?>

  <main>

  </main>
  <?php include './footer.php'; ?>

</body>
</html>
