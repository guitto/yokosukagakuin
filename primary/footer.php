<footer class="l-footer">
  <div class="l-footer_content l-flex -between">
    <div class="l-footer_contact l-flex -column">
      <div class="contact -address">
        <h5 class="c-title -sm -white">横須賀学院小学校</h5>
        <p class="c-text -white u-mt-s">〒238-8511 神奈川県横須賀市稲岡町82</p>
        <a href="https://goo.gl/maps/d7Vov5DBPTJMXmWm6"><u>Google Map</u></a>
        <p class="c-text -white">〔TEL〕046-825-1920</p>
        <p class="c-text -white">〔FAX〕046-825-1925</p>
        <p class="c-text -white">電話受付時間 7:40~18:00</p>
      </div>
      <div class="contact -buttons">
        <div class="relative">
          <a href="https://www.yokosukagakuin.ac.jp/primary/toiawase.html">
            <button class="c-button -footer -bold -color -contact -nowrap -hover1 u-mt-s">
              お問い合わせ
            </button>
          </a>
          <a href="https://www.facebook.com/Yokosukagakuinelementaryschool/">
            <img src="./wp/assets/images/img_facebook.png" alt="facebook" class="l-footer_facebook">
          </a>
        </div>
        <a href="https://netty01.lekumo.biz/primaryyokosukagakuin/">
          <button class="c-button -footer -bold -white -bgcolor -nowrap -hover1 u-mt-s">
            いきいきBlog
          </button>
        </a>
        <a href="https://www.dtps.chc.edu.tw/">
          <button class="c-button -footer -bold -white -bgcolor -nowrap -hover1 u-mt-s">
            海外姉妹校
          </button>
        </a>
      </div>
    </div>

    <nav class="l-footer_nav u-mt-m">
      <dl class="list -about">
        <dt class="title">学校について</dt>
        <dd class="item u-mt-xs"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/aisatsu.html">挨拶</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/enkakugaiyou.html">沿革</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/kyouikurinen.html">建学の精神</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/kirisutokyou1.html">キリスト教教育</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/4422.html">小中高一貫教育</a></dd>
      </dl>
      
      <dl class="list -education">
        <dt class="title">教育内容</dt>
        <dd class="item u-mt-xs"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/kisogakuryoku.html">基礎学力の充実</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/ikitakyouiku.html">生きた教育</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/English.html">English</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/taiwan.html">台湾国際交流</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/kodomotetsugaku.html">こども哲学</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/shizenkyoushitsu.html">五感で感じる教育</a></dd>
      </dl>

      <dl class="list -school">
        <dt class="title">学校生活</dt>
        <dd class="item u-mt-xs"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/ichinichi.html">1日の生活</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/nenkangyouji.html">年間行事</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/sisetsu.html">施設</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/anzenkanri.html">安全管理</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/schoolbus.html">スクールバス</a></dd>
      </dl>

      <dl class="list -admission">
        <dt class="title">入学案内</dt>
        <dd class="item u-mt-xs"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/gakkouannai.html">説明会</a></dd>
        <dd class="item"><a class="c-link -footer" href="https://www.yokosukagakuin.ac.jp/primary/nyugakushiken.html">入学・転入学試験</a></dd>
      </dl>
    </nav>

    <p class="l-footer-copyright">
      ©YOKOSUKA GAKUIN ELEMENTARY All Right Reserved.
    </p>
  </div>

</footer>
<!-- 使う場合はコメントアウト外す -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="./wp/assets/plugin/swiper/swiper.min.js"></script>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" src="./wp/assets/js/micromodal.min.js"></script>
<script type="text/javascript" src="./wp/assets/js/common.js"></script>
</body>

</html>

