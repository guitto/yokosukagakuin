# 横須賀学院小学校トップページ（部分CMS）

2021/03/31公開済み
横須賀学院小学校（https://www.yokosukagakuin.ac.jp/primary）のトップページソースです。
ページの一部（モーダルコンテンツ）をWordPressによりCMS化しています。
本番環境へは手動で直接移行している。

# 環境

CMSテスト環境：http://yokosukagakuin.test-develop.site/primary/wp/wp-admin
本番環境：http://www.yokosukagakuin.ac.jp/primary/wp/wp-admin

# メモ

サイドメニューの項目表示・非表示の切り替えはfunction.phpを参照
yokosukagakuin/primary/wp/wp-content/themes/Yokosukagakuin/functions.php

front-page.htmlはデザインを担当されたモノリスジャパン様との確認で使用。
http://test-develop.site/yokosukagakuin/coding/20210308

primary.htmlはGoogleに登録されている横須賀学院小学校のURLが
https://www.yokosukagakuin.ac.jp/primary/wp/primary.html
である為、まずhtaccessで
https://www.yokosukagakuin.ac.jp/primary/primary.html
にリダイレクトさせ、
primary.htmlから今回作成したトップページ
https://www.yokosukagakuin.ac.jp/primary/
へmeta refreshを使用しリダイレクトさせている。
